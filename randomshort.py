import webapp

formulario = """
    <form action=" " method="POST">
    <p>"URL: <input type="text" name="url">" </p>
    <p>"Short: <input type="text" name="short">" </p>
    <input type="submit" value="Enviar">
    </form>
    """


class randomshort(webapp.webApp):
    urls = {}

    def parse(self, request):
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        metodo, recurso, cuerpo = parsedRequest

        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>" + "Estas son las URLS disponibles: " + str(self.urls)  \
                           + formulario + "</body></html>"
            else:
                short = recurso.split("/")[1]
                if short in self.urls:
                    url = self.urls[short]
                    httpCode = "301 Moved Permanently"
                    htmlBody = "<html><body>" \
                               + "<meta http-equiv='refresh' content='0; URL=" + url + "'></body></html>"
                else:
                    httpCode = "404 Not Found"
                    htmlBody = "Not Found"

        elif metodo == 'POST':
            url = (cuerpo.split('&')[0].split('=')[-1])
            short = (cuerpo.split('&')[-1].split('=')[-1])

            if url == "" or short == "":
                httpCode = "404 Not Found"
                htmlBody = "Not Found"
            else:
                if not url.startswith('http://') and not url.startswith('https://'):
                    url = "https://" + url

                self.urls[short] = url
                httpCode = "200 OK"
                htmlBody = f"<html><body>Original URL: <a href='{url}'>{url}</a>" \
                           f"<br>Shorter URL: <a href='{short}'>'http://localhost:1234/' + {short}</a></body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Not Found"

        return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)
